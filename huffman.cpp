#include <bits/stdc++.h> 
#include <unordered_map>
#include <vector>
#include <fstream>
#include <cmath>


using namespace std; 

unordered_map <char,string> codewords;
unordered_map <char,int> freq ;

struct MinHeapNode { 
  
    // One of the input characters 
    char data; 
  
    // Frequency of the character 
    unsigned freq; 
  
    // Left and right child 
    MinHeapNode *left, *right; 
  
    MinHeapNode(char data, unsigned freq) 
  
    { 
  
        left = right = NULL; 
        this->data = data; 
        this->freq = freq; 
    } 
}; 
  
// For comparison of 
// two heap nodes (needed in min heap) 
struct compare { 
  
    bool operator()(MinHeapNode* l, MinHeapNode* r) 
  
    { 
        return (l->freq > r->freq); 
    } 
}; 
  
// Prints huffman codes from 
// the root of Huffman Tree. 
void printCodes(struct MinHeapNode* root, string str) 
{ 
  
    if (!root) 
        return; 
  
    if (root->data != '$') 
    {
        cout << root->data << ": " << str << "\t"<<freq[root->data]<<"\n";

        codewords[root->data] = str; 
    }
  
    printCodes(root->left, str + "0"); 
    printCodes(root->right, str + "1"); 
} 
  
// The main function that builds a Huffman Tree and 
// print codes by traversing the built Huffman Tree 
void HuffmanCodes(vector<char> data, unordered_map<char,int> freq) 
{ 
    int size = data.size();
    struct MinHeapNode *left, *right, *top; 
  
    // Create a min heap & inserts all characters of data[] 
    priority_queue<MinHeapNode*, vector<MinHeapNode*>, compare> minHeap; 
  
    for (int i = 0; i < size; ++i) 
        minHeap.push(new MinHeapNode(data[i], freq[i])); 
  
    // Iterate while size of heap doesn't become 1 
    while (minHeap.size() != 1) { 
  
        // Extract the two minimum 
        // freq items from min heap 
        left = minHeap.top(); 
        minHeap.pop(); 
  
        right = minHeap.top(); 
        minHeap.pop(); 
  
        // Create a new internal node with 
        // frequency equal to the sum of the 
        // two nodes frequencies. Make the 
        // two extracted node as left and right children 
        // of this new node. Add this node 
        // to the min heap '$' is a special value 
        // for internal nodes, not used 
        top = new MinHeapNode('$', left->freq + right->freq); 
  
        top->left = left; 
        top->right = right; 
  
        minHeap.push(top); 
    } 
  
    // Print Huffman codes using 
    // the Huffman tree built above 
    printCodes(minHeap.top(), ""); 
} 
  
// Driver program to test above functions 
int main() 
{ 
    
    vector <char> data;  

    ifstream file ("input.txt");
    string s;

    if (file.is_open())
    {
      while ( getline (file,s) )
      {
         // cout << s << '\n';
         for(int i=0;i<s.length();i++)
         {
             if(freq[s[i]] == 0)
             {
                freq[s[i]]++;
                data.push_back(s[i]);
             }

             else
                freq[s[i]]++;
         }
      }
      file.close();
    }

    cout<<"\n"<<"\n";
    int total = 0;
    for(int i=0;i<data.size();i++)
    {
         // cout<<data[i]<<":"<<freq[data[i]]<<"\n";
         total += freq[data[i]];

    }
    cout<<data.size()<<"\n";
    cout<<total<<"\n";    
  
    HuffmanCodes(data, freq); 
    
    float avglength = 0;
    float entropy = 0;
    for(int i=0;i<data.size();i++)
    {
         float pi =  float(freq[data[i]])/float(total);

         int li = codewords[data[i]].length();
         avglength += (freq[data[i]])*(li);
         entropy += (pi)*(log2(pi));
    }

    cout<<"Average length of codeword is :"<<avglength/total<<"\n"; 
    cout<<"Entropy : "<<-1*entropy<<"\n";
    return 0; 
}