type RHS = Atom.atom list

structure RHS_KEY : ORD_KEY = struct
    type ord_key =RHS
    fun  compare (x::xs,y::ys)=(case Atom.compare(x,y) of
        							EQUAL => (compare (xs,ys))
        							| GREATER => GREATER
        							| LESS => LESS)
    	| compare ([],[]) = EQUAL
    	| compare (x::xs,[]) = GREATER
    	| compare ([],y::ys) = LESS
    fun get(x:ord_key):RHS=x
end

structure RHSSet = RedBlackSetFn (RHS_KEY)
type Productions = RHSSet.set
type Rules = Productions AtomMap.map


fun makeRule (r,productionlist,rule) =
	let
		fun modify x = map (fn k=>Atom.atom k) x
		val s=map modify productionlist
		val s2=RHSSet.fromList(s)
	in
		AtomMap.insert(rule,Atom.atom r,s2)
	end

val rules:RHSSet.set AtomRedBlackMap.map=AtomMap.empty;
val rules=makeRule("S",[["E","$"]],rules)
val rules=makeRule("E",[["T","EP"]],rules)
val rules=makeRule("EP",[["+","T","EP"],[]],rules)
val rules=makeRule("T",[["N","TP"]],rules)
val rules=makeRule("TP",[[],["*","N","TP"]],rules)

fun makeSymbols(symbolslist)=
	let
		val s = map(fn k=>Atom.atom k)symbolslist
	in
		AtomSet.fromList(s)
	end
val symbols=AtomSet.empty;
val symbols=makeSymbols(["S","E","EP","T","TP"])

val f=AtomMap.empty;
val f=AtomMap.insert(f,Atom.atom "S",AtomSet.empty)
val f=AtomMap.insert(f,Atom.atom "E",AtomSet.empty)
val f=AtomMap.insert(f,Atom.atom "EP",AtomSet.empty)
val f=AtomMap.insert(f,Atom.atom "T",AtomSet.empty)
val f=AtomMap.insert(f,Atom.atom "TP",AtomSet.empty)

val tokens=AtomSet.empty;
val tokens=makeSymbols(["N","+","*","$"])



fun printMap (key,element) = 
	let
		val _ = print (Atom.toString key)
		val production_list = RHSSet.listItems(element)
		fun printProduction symtok_list =( print "{";map (fn k=>print((Atom.toString k)^" ")) symtok_list ; print "}" )
		val _ = (print " : ";map printProduction production_list;print "\n")
	in
		()
		
	end

val _ =print ("Grammar is as follows\n")
val t1 = AtomMap.appi printMap rules

val P=AtomMap.listItemsi rules


fun CalculateNullableSet(rules,N)=
	let
		fun checkElement (x::xs)=if AtomSet.member(N,x) andalso AtomSet.member(symbols,x)
		 then checkElement(xs) else false
			| checkElement []=true
		fun checkElements (x::xs)=if checkElement(x) then true else checkElements(xs)
			| checkElements [] =false
		fun addIfNot(key,element)=if checkElements (RHSSet.listItems(element)) 
				then (key) else (Atom.atom "$$")
		val P=AtomMap.listItemsi rules
		val Ntemp =map addIfNot P
		val NS=AtomSet.fromList(Ntemp)
		val NS=AtomSet.add(NS,Atom.atom "$$")
		val NS=AtomSet.delete(NS,Atom.atom "$$")
	in
		if AtomSet.equal(N,NS) then NS else CalculateNullableSet(rules,NS)
	end
val NullableSet=CalculateNullableSet (rules,AtomSet.empty)
fun t1 x=(print (Atom.toString x);print " ")

val _=print "Nullable Set is as follows\n"
val _=(map t1 (AtomSet.listItems(NullableSet));print "\n")

(*fun calculateFirstSet(rules,N)=
	let
		fun checkElement(x::xs,key)=if AtomSet.member(tokens,x) then [x]
		else if not (Atom.same(x,key)) then
				if  (AtomSet.member(N,x)) then 
					checkElements(RHSSet.listItems(AtomMap.lookup(rules,x)),key)@checkElement(xs,key)
				else checkElements(RHSSet.listItems(AtomMap.lookup(rules,x)),key)
			else []
			| checkElement([],key)=[]
		and checkElements(x::xs,key)=checkElement(x,key)@checkElements(xs,key)
			| checkElements([],key)=[]
		fun addIfNot(key,element)=AtomSet.fromList(checkElements (RHSSet.listItems(element),key))
	in
		AtomMap.mapi addIfNot rules
	end*)

(*fun calculateFirstSet(rules,N,f)=
	let
		fun checkElement(x::xs)=if AtomSet.member(tokens,x) then [x]
		else if (AtomSet.member(N,x)) then 
				(AtomSet.listItems(AtomMap.lookup(f,x)))@checkElement(xs)
				else (AtomSet.listItems(AtomMap.lookup(f,x)))
			| checkElement([])=[]
		and checkElements(x::xs)=checkElement(x)@checkElements(xs)
			| checkElements([])=[]
		fun addIfNot(key,element)=AtomSet.fromList(checkElements (RHSSet.listItems(element)))
		val fTemp=AtomMap.mapi addIfNot rules
		fun verify(key,element)=AtomSet.equal(AtomMap.lookup(f,key),element)
	in
		if (AtomMap.alli verify (fTemp)) then fTemp else calculateFirstSet(rules,N,fTemp)
	end


val firstSet=calculateFirstSet(rules,NullableSet,f)

fun printMap (key,element) = 
	let
		val _ = print (Atom.toString key)
		val production_list = AtomSet.listItems(element)
		fun printProduction symtok_list =( print "{";map (fn k=>print((Atom.toString k)^" ")) symtok_list ; print "}" )
		val _ = (print " : ";printProduction production_list;print "\n")
	in
		()
		
	end
	
val _ =print "First is as follows\n"
val _ = AtomMap.appi printMap firstSet

fun calculateFollowSet(rules,N,F,S,p)=
	let
		fun nullable(x::xs)=if AtomSet.member(tokens,x) then false
		else if AtomSet.member(N,x) then nullable(xs)
		else false
			| nullable ([])=true

		fun first(x::xs)=if AtomSet.member(tokens,x) then [x]
		else if AtomSet.member(N,x) 
			then AtomSet.listItems(AtomMap.lookup(F,x))@first(xs)
			else AtomSet.listItems(AtomMap.lookup(F,x))
			| first([])=[]

		fun checkElement(x::xs,key,mainkey)=if AtomSet.member(tokens,x) then checkElement(xs,key,mainkey)
		else if (Atom.same(x,mainkey)) then
			if nullable(xs) then
				first(xs)@AtomSet.listItems(AtomMap.lookup(S,key))
			else first(xs)
			else checkElement(xs,key,mainkey)
			| checkElement([],key,mainkey)=[]
		and checkElements(x::xs,key,mainkey)=checkElement(x,key,mainkey)@checkElements(xs,key,mainkey)
			| checkElements([],key,mainkey)=[]
		and tolist((key,element)::xs,mainkey)=checkElements(RHSSet.listItems(element),key,mainkey)@tolist(xs,mainkey)
			| tolist([],mainkey)=[]
		fun addIfNot(key,element)=AtomSet.fromList(tolist (p,key))
		val STemp =AtomMap.mapi addIfNot rules
		fun verify(key,element)=AtomSet.equal(AtomMap.lookup(S,key),element)
	in
		if (AtomMap.alli verify (STemp)) then STemp else calculateFollowSet(rules,N,F,STemp,p)
	end

val P=AtomMap.listItemsi rules
val FollowSet=calculateFollowSet(rules,NullableSet,firstSet,f,P)

val _ =print "Correct Follow is as follows\n"
val _ = AtomMap.appi printMap FollowSet*)

