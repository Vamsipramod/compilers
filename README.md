<<<<<<< HEAD
# Course on Compiler Design.

[![Build Status][status]](https://app.shippable.com/bitbucket/piyush-kurur/compilers)

This is the repository for the first course on compiler construction
that I teach at IIT Palakkad. The material available here are relevant
to the course CS3300 titled "Compiler Design" and its associated
laboratory CS3310 titled "Compiler Design Laboratory". Here you will
find notes, code samples, and other information that are relevant for
this course. Students are encouraged to use the wiki that is available
with this repository for information. We also encourage responsible
editing of the wiki.

Some important links

* [Course Repository]

* [Course Wiki]

* [Issue tracker]

* [Lab problems](lab)

For any problems regarding the course please use the issue
tracker. Remember that the issue tracker is public and any one can see
the discussions therein. This has privacy implications and you might
not want to discuss things like your grade.

## Testing your setup

We have included some tests in the source files. You can run this
using the the make command. This will ensure that you have setup your
machine correctly with the correct software for this course.to this.


```
make test   # run the tests on the sample programs here.
make clean  # cleanup the directories of temporary files.

```

## Continuous Build system.

We have setup a [_continuous integration system_ at
Shippable][shippable] where the above tests are run for every pushes
or pull requests into this repository. The [shippable
builds][shippable] are run on a docker container which is build out of
the [Dockerfile][dockerfile] given in this directory.


## Other resources.

If you are familiar with Docker you can use the docker instance built
using the [`Dockerfile`][dockerfile] in this repository to try out the
code samples in this repository. Images are available at dockerhub
under the url <https://hub.docker.com/r/piyushkurur/compilers/>


[status]: <https://api.shippable.com/projects/59800285202dac07006dad2e/badge?branch=master> "Build Status"
[Course Repository]: <https://bitbucket.org/piyush-kurur/compilers>
[Course Wiki]:       <https://bitbucket.org/piyush-kurur/compilers/wiki/Home>
[Issue tracker]:     <https://bitbucket.org/piyush-kurur/compilers/issues>
[shippable]: <https://app.shippable.com/bitbucket/piyush-kurur/compilers/> "Shippable CI page"
[dockerfile]: <Dockerfile>
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
>>>>>>> master-holder
